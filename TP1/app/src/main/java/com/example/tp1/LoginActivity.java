package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity {

    Button btnAuth;
    EditText password,login;
    TextView result;
    private static final String TAG ="Example :";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        password = (EditText)findViewById(R.id.password);
        login = (EditText) findViewById(R.id.login);
        result = (TextView) findViewById(R.id.resultView);
        btnAuth = (Button)findViewById(R.id.btnAuth);
        btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(login.getText().toString().isEmpty() || password.getText().toString().isEmpty()  ){
                    result.setText("Remplir tous les champs !!!");
                    Log.i(TAG,"lionel");
                }
                else{
                    result.setText("Valide !!!");
                }
            }
        });
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }


}