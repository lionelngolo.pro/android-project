package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    TextView loginTxt, passwordTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String login = bundle.getString("login");
        String password = bundle.getString("password");
        loginTxt = (TextView) findViewById(R.id.loginText);
        passwordTxt = (TextView) findViewById(R.id.passwordText);

        loginTxt.setText("Login :  "+login);
        passwordTxt.setText("Password :  "+password);

    }
}