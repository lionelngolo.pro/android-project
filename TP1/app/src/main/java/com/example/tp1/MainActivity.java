package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnAuth;
    EditText password,login;
    TextView result;
    private static final String TAG ="Resultat Connexion :";
    Toast t;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        password = (EditText)findViewById(R.id.password);
        login = (EditText) findViewById(R.id.login);
        result = (TextView) findViewById(R.id.resultView);
        Intent intent = new Intent(this,HomeActivity.class);
        Bundle bundle = new Bundle();
        btnAuth = (Button)findViewById(R.id.btnAuth);
        btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(login.getText().toString().isEmpty()
                        || password.getText().toString().isEmpty()
                        || !login.getText().toString().equals("abcde")
                        || !password.getText().toString().equals("lionel")  ){
                    result.setText("Remplir tous les champs !!!");
                    result.setTextColor(Color.RED);
                    t = Toast.makeText(view.getContext(),"Error",Toast.LENGTH_SHORT);
                    t.show();
                    Log.i(TAG,"Erreur lors de la saisie");
                }
                else{
                    result.setText("Valide !!!");
                    result.setTextColor(Color.GREEN);
                    t = Toast.makeText(view.getContext(),"Connexion réussie",Toast.LENGTH_SHORT);
                    t.show();
                    Log.i(TAG,"Connexion réussie");
                    bundle.putString("login",login.getText().toString());
                    bundle.putString("password",password.getText().toString());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });

    }





}